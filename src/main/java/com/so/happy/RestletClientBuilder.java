package com.so.happy;

import org.restlet.Client;
import org.restlet.data.Protocol;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/17/14
 * Time: 10:25 AM
 */
public class RestletClientBuilder {
    private RestletClientBuilder() {
    }

    private static RestletClientBuilder INSTANCE;

    public static RestletClientBuilder newBuilder() {
        if (INSTANCE == null) {
            Lock createLock = new ReentrantLock();
            createLock.lock();
            INSTANCE = new RestletClientBuilder();
            createLock.unlock();
        }
        return INSTANCE;
    }


    public Client newHttpClient() {
        final Client httpClient = new Client(Protocol.HTTP);
        return httpClient;
    }
}
