package com.so.happy;

import org.restlet.Request;
import org.restlet.data.Encoding;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Preference;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/17/14
 * Time: 10:17 AM
 * <p><code>Restlet</code> Request builder helps encapsulate the <code>Request</code> object creation.</p>
 * <p/>
 * <p>By default, the accepted media type is JSON</p>
 * <p/>
 * <p>The <code>Accept-Encoding</code> header is set to GZIP</p>
 * <p/>
 * <p>For more information please follow the <a href="https://api.stackexchange.com/docs/compression">stackoverflow API documentation</a></p>
 */
public class RestletRequestBuilder {

    private RestletRequestBuilder() {
    }

    public static RestletRequestBuilder INSTANCE;

    public static RestletRequestBuilder newBuilder() {
        if (INSTANCE == null) {
            Lock createLock = new ReentrantLock();
            createLock.lock();
            INSTANCE = new RestletRequestBuilder();
            createLock.unlock();
        }
        return INSTANCE;
    }

    public Request newGetRequest(String url) {
        Request r = new Request();
        r.setMethod(Method.GET);
        r.setResourceRef(url);
        final ArrayList<Preference<Encoding>> preferences = new ArrayList<>();
        preferences.add(new Preference<>(Encoding.GZIP));
        r.getClientInfo().setAcceptedEncodings(preferences);
        r.getClientInfo().getAcceptedMediaTypes().add(new Preference<>(MediaType.APPLICATION_JSON));
        return r;
    }
}
