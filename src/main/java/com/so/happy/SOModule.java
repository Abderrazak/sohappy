package com.so.happy;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.so.happy.impl.StackOverFlowQuestionRestlet;
import com.so.happy.spi.StackOverFlowQuestion;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/17/14
 * Time: 3:58 PM
 */
public class SOModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(StackOverFlowQuestion.class).to(StackOverFlowQuestionRestlet.class);
    }
}
