package com.so.happy.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.so.happy.RestletRequestBuilder;
import com.so.happy.model.SOItem;
import com.so.happy.model.StackOverflowEntity;
import com.so.happy.model.StackOverflowQuestionEntity;
import com.so.happy.spi.StackOverFlowQuestion;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.restlet.Request;
import org.restlet.Response;

import java.io.IOException;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/14/14
 * Time: 2:16 PM
 */
public class StackOverFlowQuestionRestlet implements StackOverFlowQuestion {

    @Override
    public StackOverflowQuestionEntity findQuestionById(String questionId) throws IOException {
        if (StringUtils.isBlank(questionId))
            throw new IllegalArgumentException("You must specify the stackoverflow question id.");
        Request r = RestletRequestBuilder.newBuilder().newGetRequest(buildQuestionURI(questionId));
        final Response response = HTTP_CLIENT.handle(r);
        //
        // TODO how to make diffrence between uncompressed and compressed streams -in case of an error object- ?
        //
        final GZIPInputStream gzipInputStream = new GZIPInputStream(response.getEntity().getStream());
        StringWriter stringWriter = new StringWriter();
        IOUtils.copy(gzipInputStream, stringWriter);
        Gson gson = new GsonBuilder().create();
        final StackOverflowEntity soEntity = gson.fromJson(stringWriter.toString(), StackOverflowEntity.class);
        System.err.println("--------------------------");
        System.err.println(soEntity);
        System.err.println("--------------------------");
        final StackOverflowQuestionEntity questionEntity = new StackOverflowQuestionEntity();
        questionEntity.setQuestionId(questionId);
        final SOItem soItem = soEntity.getItems()[0];
        questionEntity.setTitle(soItem.getTitle());
        questionEntity.setLink(soItem.getLink());
        questionEntity.setAnswered(soItem.isAnswered());
        questionEntity.setViewCount(soItem.getViewCount());
        questionEntity.setAnswerCount(soItem.getAnswerCount());
        return questionEntity;
    }

    private String buildQuestionURI(String questionId) {
        final String s = SO_BASE_API_RESOURCE_URI + SO_QUESTIONS_URI_FRAGMENT + "/" + questionId + "?key=" + SO_APPLICATION_KEY + SO_QUERY_PARAM_SITE;
        System.out.println("called Url : " + s);
        return s;
    }

}
