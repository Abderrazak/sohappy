package com.so.happy.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/17/14
 * Time: 3:41 PM
 */
public class SOItem {
    private String[] tags;
    private SOOwner owner;
    @SerializedName("is_answered")
    private boolean isAnswered;
    @SerializedName("view_count")
    private int viewCount;
    @SerializedName("answer_count")
    private int answerCount;
    private int score;
    @SerializedName("last_activity_date")
    private String lastActivityDate;
    @SerializedName("creation_date")
    private String creationDate;
    @SerializedName("last_edit_date")
    private String lastEditDate;
    @SerializedName("question_id")
    private String questionId;
    private String link;
    private String title;

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public SOOwner getOwner() {
        return owner;
    }

    public void setOwner(SOOwner owner) {
        this.owner = owner;
    }

    public boolean isAnswered() {
        return isAnswered;
    }

    public void setAnswered(boolean answered) {
        isAnswered = answered;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(String lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(String lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SOItem{" +
                "tags=" + Arrays.toString(tags) +
                ", owner=" + owner +
                ", isAnswered=" + isAnswered +
                ", viewCount=" + viewCount +
                ", answerCount=" + answerCount +
                ", score=" + score +
                ", lastActivityDate=" + lastActivityDate +
                ", creationDate=" + creationDate +
                ", lastEditDate=" + lastEditDate +
                ", questionId='" + questionId + '\'' +
                ", link='" + link + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
