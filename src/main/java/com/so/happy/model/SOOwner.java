package com.so.happy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/17/14
 * Time: 3:41 PM
 */
public class SOOwner {

    private String reputation;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_type")
    private String userType;
    @SerializedName("accept_rate")
    private String acceptRate;
    @SerializedName("profile_image")
    private String profileImage;
    @SerializedName("display_name")
    private String displayName;
    private String link;

    public String getReputation() {
        return reputation;
    }

    public void setReputation(String reputation) {
        this.reputation = reputation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAcceptRate() {
        return acceptRate;
    }

    public void setAcceptRate(String acceptRate) {
        this.acceptRate = acceptRate;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "SOOwner{" +
                "reputation='" + reputation + '\'' +
                ", userId='" + userId + '\'' +
                ", userType='" + userType + '\'' +
                ", acceptRate='" + acceptRate + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", displayName='" + displayName + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
