package com.so.happy.model;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/13/14
 * Time: 3:19 PM
 */
public class StackOverflowEntity {

    private SOItem[] items;
    private boolean hasMore;
    private int quotaMax;
    private int quotaRemaining;

    public SOItem[] getItems() {
        return items;
    }

    public void setItems(SOItem[] items) {
        this.items = items;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    public int getQuotaMax() {
        return quotaMax;
    }

    public void setQuotaMax(int quotaMax) {
        this.quotaMax = quotaMax;
    }

    public int getQuotaRemaining() {
        return quotaRemaining;
    }

    public void setQuotaRemaining(int quotaRemaining) {
        this.quotaRemaining = quotaRemaining;
    }

    @Override
    public String toString() {
        return "StackOverflowEntity{" +
                "items=" + Arrays.toString(items) +
                ", hasMore=" + hasMore +
                ", quotaMax=" + quotaMax +
                ", quotaRemaining=" + quotaRemaining +
                '}';
    }
}
