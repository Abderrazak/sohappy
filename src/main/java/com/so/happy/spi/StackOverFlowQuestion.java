package com.so.happy.spi;

import com.so.happy.RestletClientBuilder;
import com.so.happy.model.StackOverflowQuestionEntity;
import org.restlet.Client;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/14/14
 * Time: 2:13 PM
 */
public interface StackOverFlowQuestion {

    final Client HTTP_CLIENT = RestletClientBuilder.newBuilder().newHttpClient();
    String SO_BASE_API_RESOURCE_URI = "https://api.stackexchange.com/2.2";
    String SO_QUESTIONS_URI_FRAGMENT = "/questions";
    String SO_QUERY_PARAM_SITE = "&site=stackoverflow";
    String SO_APPLICATION_KEY = "6D42bNy8SqNQOU6TeJ1Upg((";

    /**
     * @param questionId
     * @return {@linkplain StackOverflowQuestionEntity} if found encapsulating the main question attributes
     * @throws IOException
     */
    StackOverflowQuestionEntity findQuestionById(String questionId) throws IOException;
}
