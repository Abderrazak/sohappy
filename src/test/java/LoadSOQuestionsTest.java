import com.google.inject.Guice;
import com.google.inject.Injector;
import com.so.happy.SOModule;
import com.so.happy.model.StackOverflowQuestionEntity;
import com.so.happy.spi.StackOverFlowQuestion;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 2/13/14
 * Time: 3:07 PM
 */
public class LoadSOQuestionsTest {

    private static final Injector INJECTOR = Guice.createInjector(new SOModule());

    @Test
    public void shouldBeAbleToLoadFirst100SOQuestionsByIndicatedTagRestlet() throws IOException {
        final StackOverFlowQuestion happy = INJECTOR.getInstance(StackOverFlowQuestion.class);
        final String expectedQuestionId = "21733439";
        final StackOverflowQuestionEntity question = happy.findQuestionById(expectedQuestionId);
        Assertions.assertThat(question.getQuestionId()).isNotEmpty().isEqualTo(expectedQuestionId);
    }
}
